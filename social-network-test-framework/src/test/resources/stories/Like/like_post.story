Like a post in TravelPoint

Meta:
@category like

Narrative:

In order to show like functionality
As a user
I will log in
Navigate to newsfeed and like the first post without any likes

Scenario: Like post
Given I am logged in
When I navigate to newsfeed page
When I give a like to the first post found without any likes
Then Post likes are increment by one