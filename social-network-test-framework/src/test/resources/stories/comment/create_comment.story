Create a comment in TravelPoint

Meta:
@category comment

Narrative:

In order to show comment functionality
As a user
I will log in
Navigate to newsfeed and create a comment

Scenario: Create comment
Given I am logged in
When I navigate to newsfeed page
And I type text in comment input field
And I press ENTER to publish the comment
Then the comment is published