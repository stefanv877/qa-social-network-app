Login to TravelPoint

Meta:
@category login

Narrative:

In order to show login functionality
As a user
I want to login successful

Scenario: Login with valid credentials
Given I am on the login page
When I enter my username and password
And click on sign in button
Then the user is logged in and logout button is displayed