Register to TravelPoint

Meta:
@category registration

Narrative:

In order to show registration functionality
As a user
I want to register successful

Scenario: Register with valid credentials
Given I am on the landing page
When I register a valid account
Then login page is loaded and sign in button is displayed