Search user by email in TravelPoint

Meta:
@category search

Narrative:

In order to show search user functionality
As a user
I will log in
Navigate to home page and search for user by his email address

Scenario: Search user by email
Given I am logged in
When When I type email of jon in search field
Then Email of jon should appear in search result