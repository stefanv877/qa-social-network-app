Create a post with more than 255 character in TravelPoint

Meta:
@category post

Narrative:
In order to test post content length validation
As a user
I want to create a post with more than the maximum character allowed

Scenario: Insert content in post field with more than 255 character
Given I am logged in
When I navigate to newsfeed page
And I type text with more than 255 characters in post text area
And I click the publish button
Then the post is published and trimmed to 255 characters