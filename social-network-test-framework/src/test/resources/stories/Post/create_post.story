Create a post in TravelPoint

Meta:
@category post

Narrative:
In order to show post functionality
As a user
I want to create a post

Scenario: Create post
Given I am logged in
When I navigate to newsfeed page
And I type text in post text area
And I click the publish button
Then the post is published