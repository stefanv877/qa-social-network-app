Update user info

Meta:
@category user

Narrative:
In order to show update user info functionality
As a user
I want to update my email, first name and last name

Scenario: Update user info
Given I am logged in
And I am on My Profile Info page
When I click on Update Profile button
And Update Profile page is displayed
When I update my email address
And use my current password
And update my first name
And update my last name
And save all changes
Then my personal information is updated