package stepdefinitions;


import com.telerikacademy.testframework.Utils;
import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.annotations.BeforeStories;


public class BaseStepDefinitions {

    @BeforeStories
    public void beforeStories(){
        Utils.loadBrowser();
    }

    @AfterStories
    public void afterStories(){
        Utils.tearDownWebDriver();
    }
}
