package stepdefinitions;


import com.telerikacademy.testframework.Utils;
import com.telerikacademy.testframework.pages.*;
import org.apache.commons.lang.RandomStringUtils;
import org.assertj.core.api.SoftAssertions;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class StepDefinitions extends BaseStepDefinitions{

    private static final String UPDATED = "updated";

    SoftAssertions softAssertions = new SoftAssertions();

    private Login login = new Login();
    private LandingPage landingPage = new LandingPage();
    private Newsfeed newsfeed = new Newsfeed();
    private Home home = new Home();
    private UserAbout userAbout = new UserAbout();
    private UserUpdate userUpdate = new UserUpdate();

    private String randomString = RandomStringUtils.randomAlphabetic(5).toLowerCase();
    private String randomString256chars = RandomStringUtils.randomAlphabetic(256).toLowerCase();
    private String postContent = randomString + " POST";
    private String postContent256chars = randomString256chars;
    private String commentContent = randomString + "COMMENT";

    // GENERAL PURPOSE STEPS

    @Given("I am logged in")
    public void logIn() {
        landingPage.clickAlreadyHaveAnAccountLink();
        login.setUsername(Utils.getConfigPropertyByKey("admin.user"));
        login.setPassword(Utils.getConfigPropertyByKey("admin.password"));
        login.clickSignIn();
    }

    // LOGIN STEPS

    @Given("I am on the login page")
    public void goToLoginPage() {
        landingPage.clickAlreadyHaveAnAccountLink();
    }

    @When("I enter my username and password")
    public void enterUsernameAndPassword() {
        login.setUsername(getConfigPropertyByKey("admin.user"));
        login.setPassword(getConfigPropertyByKey("admin.password"));
    }

    @When("click on sign in button")
    public void clickSignIn() {
        login.clickSignIn();
    }

    @Then("the user is logged in and logout button is displayed")
    public void checkForLogoutButton() {
        softAssertions.assertThat(login.logoutButtonIsDisplayed());
        login.logout();
    }


    // REGISTER STEPS

    //we can update go function with value in body?
    @Given("I am on the landing page")
    public void checkForSignUpButton() {
        softAssertions.assertThat(landingPage.signUpButtonIsDisplayed());
    }

    @When("I register a valid account")
    public void registerValidAccount(){
        String uuid = "user" + randomString;
        landingPage.setUsername(uuid);
        landingPage.setEmail(uuid + "@mydomain.com");
        landingPage.setPassword("123");
        landingPage.setPasswordConfirmation("123");
        landingPage.setFirstName(uuid);
        landingPage.setLastName(uuid);
        landingPage.clickSignUpButton();
    }

    @Then("login page is loaded and sign in button is displayed")
    public void checkForSignInButton() {
        softAssertions.assertThat(login.signInButtonIsDisplayed());
        login.setUsername(getConfigPropertyByKey("admin.user"));
        login.setPassword(getConfigPropertyByKey("admin.password"));
        login.clickSignIn();
        home.logout();
    }


    // POST STEPS

    @When("I navigate to newsfeed page")
    public void navigateToNewsfeed(){
        home.navigateToNewsfeedPage();
    }

    @When("I type text in post text area")
    public void typeTextInPostTextArea() {
        newsfeed.typeTextInPostTextArea(postContent);
    }


    @When("I click the publish button")
    public void clickPublishButton() {
        newsfeed.clickPublishButton();
    }

    @Then("the post is published")
    public void postIsPublished() {
        softAssertions.assertThat(newsfeed.postText()).isEqualTo(postContent);
        newsfeed.logout();
        softAssertions.assertAll();
    }

    @When("I type text with more than 255 characters in post text area")
    public void typeTextInPostTextAreaWith256chars() {
        newsfeed.typeTextInPostTextArea(postContent256chars);
    }

    @Then("the post is published and trimmed to 255 characters")
    public void postContentIsTrimmedTo255chars() {
        softAssertions.assertThat(newsfeed.postContentIsTrimmedTo255chars());
        newsfeed.logout();
        softAssertions.assertAll();
    }

    // COMMENT STEPS

    @When("I type text in comment input field")
    public void typeTextInCommentInputField(){
        newsfeed.typeInCommentInputField(commentContent);
    }

    @When("I press ENTER to publish the comment")
    public void publishCommentInPost(){
        newsfeed.publishComment();
    }

    @Then("the comment is published")
    public void commentIsPublished(){
        softAssertions.assertThat(newsfeed.commentText()).isEqualTo(commentContent);
        newsfeed.logout();
        softAssertions.assertAll();
    }

    // LIKE POST

    @When("I give a like to the first post found without any likes")
    public void likePost(){
        newsfeed.likeFirstPostWithZeroLikes();
    }

    @Then("Post likes are increment by one")
    public void confirmPostIsLiked(){
        softAssertions.assertThat(newsfeed.likeFirstPostWithZeroLikesTrue());
        newsfeed.logout();
        softAssertions.assertAll();
    }

    //SEARCH USER

    @When("When I type email of $user in search field")
    public void searchUserByEmail(String user) {
        home.typeValueInSearchField(getConfigPropertyByKey("email." + user));
    }

    @Then("Email of $user should appear in search result")
    public void getSearchResult(String user) {
        softAssertions.assertThat(getConfigPropertyByKey("email." + user).equals(home.getUserEmailFromSearchResult()));
        home.logout();
        softAssertions.assertAll();
    }

    // USER STEPS

    @Given("I am on My Profile Info page")
    public void navigateToMyProfilepage() {
        home.navigateToMyInfoPage();
    }

    @When("I click on Update Profile button")
    public void clickUpdateProfileButton() {
        userAbout.clickUpdateProfileButton();
    }

    @When("Update Profile page is displayed")
    public void updateProfilePageIsDisplayed() {
        softAssertions.assertThat(userUpdate.titleIsDisplayed());
        softAssertions.assertAll();
    }

    @When("I update my email address")
    public void updateEmailAddress() {
        userUpdate.setEmail(UPDATED);
    }

    @When("use my current password")
    public void useCurrentPassword() {
        userUpdate.setPassword(getConfigPropertyByKey("admin.password"));
        userUpdate.setConfirmPassword(getConfigPropertyByKey("admin.password"));
    }

    @When("update my first name")
    public void updateFirstName() {
        userUpdate.setFirstName(UPDATED);
    }

    @When("update my last name")
    public void updateLastName() {
        userUpdate.setLastName(UPDATED);
    }

    @When("save all changes")
    public void updateProfile() {
        userUpdate.clickUpdateProfileButton();
    }

    @Then("my personal information is updated")
    public void informationIsUpdated() {
        home.navigateToMyInfoPage();
        softAssertions.assertThat(userAbout.getEmail()).contains(UPDATED);
        softAssertions.assertThat(userAbout.getFirstName()).contains(UPDATED);
        softAssertions.assertThat(userAbout.getLastName()).contains(UPDATED);
        home.logout();
        softAssertions.assertAll();
    }
}
