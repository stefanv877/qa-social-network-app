package com.telerikacademy.testframework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UserUpdate extends BasePage {

    @FindBy(xpath = "//div/h4[contains(text(), 'Information')]")
    private WebElement title;

    @FindBy(id = "email")
    private WebElement emailField;

    @FindBy(id = "password")
    private WebElement passwordField;

    @FindBy(id = "confirmPassword")
    private WebElement confirmPasswordField;

    @FindBy(id = "firstName")
    private WebElement firstNameField;

    @FindBy(id = "lastName")
    private WebElement lastNameField;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement updateProfileButton;

    public UserUpdate() {
        super();
        PageFactory.initElements(driver, this);
    }

    public String getTitle() {
        waitVisibility(title);
        return title.getText();
    }

    public boolean titleIsDisplayed() {
        return title.isDisplayed();
    }

    public void setEmail(String text) {
        waitVisibility(emailField);
        emailField.sendKeys(text);
    }

    public void setPassword(String text) {
        waitVisibility(passwordField);
        passwordField.sendKeys(text);
    }

    public void setConfirmPassword(String text) {
        waitVisibility(confirmPasswordField);
        confirmPasswordField.sendKeys(text);
    }

    public void setFirstName(String text) {
        waitVisibility(firstNameField);
        firstNameField.sendKeys(text);
    }

    public void setLastName(String text) {
        waitVisibility(lastNameField);
        lastNameField.sendKeys(text);
    }

    public void clickUpdateProfileButton() {
        waitToBeClickable(updateProfileButton);
        clickButton(updateProfileButton);
    }
}
