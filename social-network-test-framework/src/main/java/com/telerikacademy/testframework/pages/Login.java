package com.telerikacademy.testframework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class Login extends BasePage {

    @FindBy(id = "example-username")
    private WebElement usernameField;

    @FindBy(id = "example-password")
    private WebElement passwordField;

    @FindBy(id = "sign")
    private WebElement signInButton;

    @FindBy(xpath = "//a[text()='Logout']")
    private WebElement logoutButton;

    public Login() {
        super();
        PageFactory.initElements(driver, this);
    }

    public void setUsername(String username) {
        waitToBeClickable(signInButton);
        usernameField.sendKeys(username);
    }

    public void setPassword(String password) {
        waitVisibility(signInButton);
        passwordField.sendKeys(password);
    }

    public void clickSignIn() {
        waitToBeClickable(signInButton);
        clickButton(signInButton);
    }

    public boolean logoutButtonIsDisplayed() {
        waitToBeClickable(logoutButton);
        return logoutButton.isDisplayed();
    }

    public boolean signInButtonIsDisplayed() {
        waitVisibility(signInButton);
        return signInButton.isDisplayed();
    }
}

