package com.telerikacademy.testframework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class LandingPage extends BasePage {

    @FindBy(name = "username")
    private WebElement usernameField;

    @FindBy(id = "example-email")
    private WebElement emailField;

    @FindBy(id = "example-password")
    private WebElement passwordField;

    @FindBy(id = "example-password-confirmation")
    private WebElement passwordConfirmationField;

    @FindBy(id = "example-first-name")
    private WebElement firstNameField;

    @FindBy(id = "example-last-name")
    private WebElement lastNameField;

    @FindBy(xpath = "//label[@for='example-user-photo']")
    private WebElement userPhotoField;

    @FindBy(xpath = "//label[@for='example-user-cover-photo']")
    private WebElement userCoverPhotoField;

    @FindBy(xpath = "//button[text()='Sign up']")
    private WebElement signUpButton;

    @FindBy(id = "show-login")
    private WebElement alreadyHaveAnAccountLink;

    public LandingPage() {
        super();
        PageFactory.initElements(driver, this);
    }

    public void go() {
        driver.get("http://localhost:8080");
    }

    public void setUsername(String username) {
        waitVisibility(usernameField);
        usernameField.sendKeys(username);
    }

    public void setEmail(String email) {
        emailField.sendKeys(email);
    }

    public void setPassword(String password) {
        passwordField.sendKeys(password);
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        passwordConfirmationField.sendKeys(passwordConfirmation);
    }

    public void setFirstName(String firstName) {
        firstNameField.sendKeys(firstName);
    }

    public void setLastName(String lastName) {
        lastNameField.sendKeys(lastName);
    }

    public void setUserPhoto(String userPhotoPath) {
        userPhotoField.sendKeys(userPhotoPath);
    }

    public void setUserCoverPhoto(String userCoverPath) {
        userCoverPhotoField.sendKeys(userCoverPath);
    }

    public void clickSignUpButton() {
        clickButton(signUpButton);
    }

    public void clickAlreadyHaveAnAccountLink() {
        waitToBeClickable(alreadyHaveAnAccountLink);
        clickButton(alreadyHaveAnAccountLink);
    }


    public boolean signUpButtonIsDisplayed() {
        waitVisibility(signUpButton);
        return signUpButton.isDisplayed();
    }

}
