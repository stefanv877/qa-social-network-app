package com.telerikacademy.testframework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Home extends BasePage {

    @FindBy(xpath = "//div[@id='users-container']/div/div[@class='live-activity']/p/span[2]")
    private WebElement searchResultUserEmail;

    public Home() {
        super();
    }

    public void typeValueInSearchField(String value) {
        waitVisibility(searchBar);
        searchBar.sendKeys(value);
    }

    public String getUserEmailFromSearchResult() {
        waitVisibility(searchBar);
        return searchResultUserEmail.getText();
    }

}
