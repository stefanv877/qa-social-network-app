package com.telerikacademy.testframework.pages;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
    final WebDriver driver;
    private WebDriverWait wait;
    private Actions actions;
    private JavascriptExecutor executor;

    // Navigation Menu Elements
    @FindBy(xpath = "//ul[@class='nav navbar-nav navbar-right main-menu']/li/a[text()='Home']")
    private WebElement homePageButton;

    @FindBy(xpath = "//ul[@class='nav navbar-nav navbar-right main-menu']/li/a[text()='Newsfeed ']")
    private WebElement newsfeedDropDownButton;

    @FindBy(xpath = "//ul[@class='nav navbar-nav navbar-right main-menu']/li/ul/li/a[text()='Newsfeed']")
    private WebElement newsfeedPageButton;

    @FindBy(xpath = "//ul[@class='nav navbar-nav navbar-right main-menu']/li/a[text()='My friends']")
    private WebElement myFriendPageButton;

    @FindBy(xpath = "//ul[@class='nav navbar-nav navbar-right main-menu']/li/a[text()=' My Profile ']")
    private WebElement myProfileDropDownButton;

    @FindBy(xpath = "//ul[@class='nav navbar-nav navbar-right main-menu']/li/ul/li/a[text()='My info']")
    private WebElement myInfoPageButton;

    @FindBy(xpath = "//ul[@class='nav navbar-nav navbar-right main-menu']/li/a[text()='Logout']")
    private WebElement logoutButton;

    @FindBy(xpath = "//input[@class='form-control js-user-search']")
    WebElement searchBar;


    // Constructor
    public BasePage() {
        this.driver = Utils.getWebDriver();
        this.executor = (JavascriptExecutor) driver;
        wait = new WebDriverWait(this.driver, 15);
        actions = new Actions(driver);
        PageFactory.initElements(driver, this);
    }

    public void refreshPage() {
        driver.navigate().refresh();
    }

    //Wait Wrapper Method
    public void waitVisibility(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitToBeClickable(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void clickButton(WebElement element) {
        executor.executeScript("arguments[0].style.visibility='hidden'", element);
        executor.executeScript("arguments[0].click();", element);
    }

    public void pressEnter(WebElement element) {
        element.sendKeys(Keys.ENTER);
    }

    // Navigation Menu Methods
    public void navigateToHomePage() {
        waitVisibility(homePageButton);
        clickButton(homePageButton);
    }

    public void navigateToNewsfeedPage(){
        waitVisibility(newsfeedDropDownButton);
        clickButton(newsfeedDropDownButton);
        waitVisibility(newsfeedPageButton);
        clickButton(newsfeedPageButton);
    }

    public void navigateToMyFriendPage() {
        waitVisibility(myFriendPageButton);
        clickButton(myFriendPageButton);
    }

    public void navigateToMyInfoPage(){
        waitVisibility(myProfileDropDownButton);
        clickButton(myProfileDropDownButton);
        waitVisibility(myInfoPageButton);
        clickButton(myInfoPageButton);
    }

    public void logout() {
        waitVisibility(logoutButton);
        clickButton(logoutButton);
    }

    public void setSearchInput(String value) {
        waitVisibility(searchBar);
        searchBar.sendKeys(value);
    }
}
