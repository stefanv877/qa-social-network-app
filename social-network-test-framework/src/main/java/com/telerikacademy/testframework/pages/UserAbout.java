package com.telerikacademy.testframework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UserAbout extends BasePage {

    @FindBy(xpath = "//li/a[text()='About']")
    private WebElement aboutButton;

    @FindBy(xpath = "//li/a[@id='updateUser']")
    private WebElement updateProfileButton;

    @FindBy(xpath = "//div[@class='organization']/div/h5[text()='First name']/following-sibling::p")
    private WebElement firstName;

    @FindBy(xpath = "//div[@class='organization']/div/h5[text()='Last name']/following-sibling::p")
    private WebElement lastName;

    @FindBy(xpath = "//div[@class='organization']/div/h5[text()='Email']/following-sibling::p")
    private WebElement email;

    @FindBy(xpath = "//div[@class='organization']/div/h5[text()='Friends']/following-sibling::p")
    private WebElement friendsCount;

    @FindBy(xpath = "//div[@class='organization']/div/h5[text()='Posts']/following-sibling::p")
    private WebElement postsCount;

    public UserAbout() {
        super();
        PageFactory.initElements(driver, this);
    }

    public void clickAboutButton() {
        waitToBeClickable(aboutButton);
        clickButton(aboutButton);
    }

    public void clickUpdateProfileButton() {
        waitToBeClickable(updateProfileButton);
        clickButton(updateProfileButton);
    }

    public String getFirstName() {
        waitVisibility(firstName);
        return firstName.getText();
    }

    public String getLastName() {
        waitVisibility(lastName);
        return lastName.getText();
    }

    public String getEmail() {
        waitVisibility(email);
        return email.getText();
    }

    public int getFriendsCount() {
        waitVisibility(friendsCount);
        return Integer.parseInt(friendsCount.getText());
    }

    public int getPostsCount() {
        waitVisibility(postsCount);
        return Integer.parseInt(postsCount.getText());
    }
}
