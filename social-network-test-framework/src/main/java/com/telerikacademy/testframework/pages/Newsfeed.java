package com.telerikacademy.testframework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import java.util.List;

public class Newsfeed extends BasePage {

    @FindBy(xpath = "//textarea[@id='exampleTextarea']")
    private WebElement publishPostTextArea;

    @FindBy(xpath = "//div/label/select[@id='post']")
    private WebElement visibilityDropDownMenu;

    @FindBy(id = "publish_post")
    private WebElement publishPostButton;

    @FindBy(id = "post_user_details")
    private WebElement postUser;

    @FindBy(xpath = "//div[@class='post-text']/ul/li")
    private WebElement postText;

    @FindBy(xpath = "//div/label/select[@id='post']/option[@value='false']")
    private WebElement privatePost;

    @FindBy(xpath = "//div/form/div[@id='visiblePost']/label/select[@id='post']")
    private Select visibilityDropDown;

    @FindBy(xpath = "//div[@class='post-container']/div[@class='post-detail']/div[@class='post-comment']/input")
    private WebElement commentInputField;

    @FindBy(xpath = "//div[@class='post-text']/div[@class='post-text']/p/span")
    private WebElement commentText;

    @FindBy(xpath = "//div[@class='post-detail']/div[@class='reaction']/a")
    private WebElement likePostButton;

    @FindBy(xpath = "//div[@class='post-detail']/div[@class='reaction']/a")
    private List<WebElement>  listLikePostButton;

    @FindBy(id = "next-btn")
    private WebElement postNextPage;

    @FindBy(xpath = "//div[@id='delete1']")
    private WebElement veryFirstPostInNewsfeed;

    private String postLikeID;
    private final int postContentMaxLength = 255;

    public Newsfeed() {
        super();
        PageFactory.initElements(driver, this);
    }

    public void typeTextInPostTextArea(String text) {
        waitVisibility(publishPostTextArea);
        publishPostTextArea.sendKeys(text);
    }

    public void clickPublishButton() {
        clickButton(publishPostButton);
    }

    public String postText() {
        refreshPage();
        waitVisibility(postText);
        return postText.getText();
    }

    public void typeInCommentInputField(String text) {
        waitVisibility(commentInputField);
        commentInputField.sendKeys(text);
    }

    public void publishComment() {
        pressEnter(commentInputField);
    }

    public String commentText(){
        refreshPage();
        return commentText.getText();
    }

    public void clickLikePostButton(){
        waitVisibility(likePostButton);
        clickButton(likePostButton);
    }

    public void clickNextPageButton() {
        waitVisibility(postNextPage);
        clickButton(postNextPage);
    }

    public void likeFirstPostWithZeroLikes(){
        waitVisibility(postNextPage);
        int postLikeNumbers;
        int count = 0;
        for (WebElement webElement : listLikePostButton) {
            postLikeNumbers = Integer.parseInt(webElement.getText().trim());
            postLikeID = webElement.getAttribute("id");
            if (postLikeNumbers == 0) {
                System.out.println(postLikeID);
                clickButton(webElement);
                break;
            }
            count = count + 1;
        }

        if (count == 3) {
            clickNextPageButton();
            waitVisibility(veryFirstPostInNewsfeed);
            for (WebElement webElementNextPage : listLikePostButton) {
                postLikeNumbers = Integer.parseInt(webElementNextPage.getText().trim());
                postLikeID = webElementNextPage.getAttribute("id");
                if (postLikeNumbers == 0) {
                    clickButton(webElementNextPage);
                    break;
                }
            }
        }
    }

    public boolean likeFirstPostWithZeroLikesTrue() {
        int postLikeNumbers = 0;
        for (WebElement webElement: listLikePostButton) {
            if (webElement.getAttribute("id").equals(postLikeID)) {
                postLikeNumbers = Integer.parseInt(webElement.getText().trim());
                System.out.println(postLikeID);
                break;
            }
        }
        return postLikeNumbers == 1;
    }

    public boolean postContentIsTrimmedTo255chars() {
        return postText().length() == postContentMaxLength;
    }
}

