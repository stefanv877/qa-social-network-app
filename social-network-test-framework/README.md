# TravelPoint BDD Test Framework

The framework is using JBehave and Selenium against social media website for traveling (TravelPoint) which need to be hosted locally (http://localhost:8080).

# Building with Maven

The project can be runned via:

- run_tests.bat file

- using CLI command in the main directory where the pom.xml is located:
mvn clean install test -Dtest=runner/Runner.java

- using IDE
run src/test/java/runner/Runner.java

# Requirements

Building this project has been tested with Maven 3.8.1 and JDK 1.8.