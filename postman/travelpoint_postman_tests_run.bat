@echo off
setlocal EnableDelayedExpansion
rem ...
if "%$ecbId%" == "" (
	echo Please select how you would like to filter the tests:
    echo Enter '1' if you want to execute the whole collection in one run
    echo Enter '2' if you want to filter based on User functionality
    echo Enter '3' if you want to filter based on Post functionality
    echo Enter '4' if you want to filter based on Comment functionality
    echo Enter anything else to abort
    echo.
    set "UserChoice=abort"
    set /P "UserChoice=Type input: "
    if "!UserChoice!"=="1" (
        echo The whole collection will be executed
        newman run https://api.getpostman.com/collections/8121533-ed6bdc7d-53a4-41bc-a67c-45e5b82f8fb0?apikey=PMAK-5dcd4179f10f6e0036b29a8c-3cadbca48c720a0c74221b51b8dff4c8a2 -e https://api.getpostman.com/environments/8121533-c4466066-6ac8-4d58-86c0-9acbdf7834c7?apikey=PMAK-5dcd4179f10f6e0036b29a8c-3cadbca48c720a0c74221b51b8dff4c8a2 -r htmlextra --reporter-htmlextra-darkTheme
    ) else if "!UserChoice!"=="2" (
    	echo Executing Users functionality tests only: Register a user / Edit user / Delete user
    	newman run --folder Users https://api.getpostman.com/collections/8121533-ed6bdc7d-53a4-41bc-a67c-45e5b82f8fb0?apikey=PMAK-5dcd4179f10f6e0036b29a8c-3cadbca48c720a0c74221b51b8dff4c8a2 -e https://api.getpostman.com/environments/8121533-c4466066-6ac8-4d58-86c0-9acbdf7834c7?apikey=PMAK-5dcd4179f10f6e0036b29a8c-3cadbca48c720a0c74221b51b8dff4c8a2 -r htmlextra --reporter-htmlextra-darkTheme
    ) else if "!UserChoice!"=="3" (
    	echo Executing Post functionality tests only: Create post / Delete post
    	newman run --folder Post https://api.getpostman.com/collections/8121533-ed6bdc7d-53a4-41bc-a67c-45e5b82f8fb0?apikey=PMAK-5dcd4179f10f6e0036b29a8c-3cadbca48c720a0c74221b51b8dff4c8a2 -e https://api.getpostman.com/environments/8121533-c4466066-6ac8-4d58-86c0-9acbdf7834c7?apikey=PMAK-5dcd4179f10f6e0036b29a8c-3cadbca48c720a0c74221b51b8dff4c8a2 -r htmlextra --reporter-htmlextra-darkTheme
    ) else if "!UserChoice!"=="4" (
    	echo Executing Comment functionality tests only: Create comment / Delete comment / Edit comment
    	newman run --folder Comment https://api.getpostman.com/collections/8121533-ed6bdc7d-53a4-41bc-a67c-45e5b82f8fb0?apikey=PMAK-5dcd4179f10f6e0036b29a8c-3cadbca48c720a0c74221b51b8dff4c8a2 -e https://api.getpostman.com/environments/8121533-c4466066-6ac8-4d58-86c0-9acbdf7834c7?apikey=PMAK-5dcd4179f10f6e0036b29a8c-3cadbca48c720a0c74221b51b8dff4c8a2 -r htmlextra --reporter-htmlextra-darkTheme
    ) else (
        echo Unknown input ... Aborting script
        endlocal
        exit /B 400
    )
)
endlocal