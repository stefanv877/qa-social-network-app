# TravelPoint: POSTMAN API Automated Tests

TravelPoint is a Traveling Social Network that allows people with similar interests to come together and share information. You can also use it to stay connected with friends, family, colleagues, customers, or clients.

## Getting started

These instructions will give you all the information needed to prepare, execute and analyze the API tests and their results on your local machine

### Prerequisites

```
Node.js
newman
newman-reporter-htmlextra
```

### Installing

To run Newman, ensure that you have Node.js >= v6. [Install Node.js via package manager.](https://nodejs.org/en/download/package-manager/)

After you install Node.js, Newman is just a command away. Install Newman from npm globally on your system, which allows you to run it from anywhere.

```
$ npm install -g newman
```

Install newman-reporter-htmlextra  

```
$ npm install -g newman-reporter-htmlextra
```



## Running the tests

In order to run the automated test you will need to download batch file located in the current folder. 

```
travelpoint_postman_tests_run.bat
```

You can run the file by double clicking on it. 
Once you run the file you will be asked to select one of the following options:

```
Please select how you would like to filter the tests:
Enter '1' if you want to execute the whole collection in one run
Enter '2' if you want to filter based on User functionality
Enter '3' if you want to filter based on Post functionality
Enter '4' if you want to filter based on Comment functionality
Enter anything else to abort
```

Based on the option selected one of the following will be executed:

1. The whole collection containing all tests

2. User functionality test only containing these sub folders:
 - Register a user;
 - Edit user;
 - Delete user;
3. Post functionality test only containing these sub folders:
 - Create post;
 - Delete post;
4. Comment functionality tests only containing these sub folders:
 - Create comment;
 - Delete comment;
 - Edit comment;

Anything else will stop the execution and close the program.

Once the job is finished a new folder called newman will be created in the same folder where the script was executed containing HTML report with the results of the execution.