# TravelPoint Quality Assurance Processs

The main aim of having a Quality Assurance process is to ensure that the product is built right, the first time. That means, we need to ensure that the requirements are correctly defined and the development team have a solid understanding of the functionality of each features before starting to code.

Testing should support development and so testing activities are in parallel to the development activities, and at every stage of the development process we need to ensure that the code is thoroughly tested.


## 1. Purpose of this document
 
The purpose of this document is to give visibility of the whole process of the QA Team, the results achieved and the conclusions reached. 

## 2. Deliverables

Below you will find list of all deliverbales provided
	
### 2.1 Test Plan

A test plan document is a blueprint for carrying out testing process. It lists the sequence of
activities that are followed by the team to test the software. Apart from defining the testing process, it
also identifies the scope and objective of testin;

**Name**: TravelPoint_Test_Plan.pdf

**URL**: [qa-social-network-app/TravelPoint_Test_Plan.pdf](https://gitlab.com/stefanv877/qa-social-network-app/blob/master/TravelPoint_Test_Plan.pdf)

### 2.2 Test Cases

The test case document enlists the various combinations of input and output that
produces a pass or fail report. Based on the report, further rectifications or updates are decided upon;

Developing and maintaining the Test Cases was done with a web-based test managment tool called TestRail. It is used by testers, developers and other stake holders to manage, track and organize software testing efforts. It follows a centralized test management concept that helps in easy communication and enables rapid development of task across QA team and other stakeholders .

**URL**: [Test Cases](https://howtoqa.testrail.io/index.php?/suites/view/3&group_by=cases:section_id&group_order=asc)

### 2.3 Test Scripts

Test script defines the various steps as well as instructions that are executed by the team
on the software to test and validate its functionality and to ensure that it is as per the requirements and
expectations of the client;

**POSTMAN**: [qa-social-network-app/postman](https://gitlab.com/stefanv877/qa-social-network-app/tree/master/postman)


**Java/Selenium/JBehaver**: [qa-social-network-app/social-network-test-framework](https://gitlab.com/stefanv877/qa-social-network-app/tree/master/social-network-test-framework)

 
### 2.3 Test Data

The data or input provided to the application with the intent of fetching some results, are
called test data;

| USERNAMES | EMAILS | PASSWORDS | FIRST NAMES | LAST NAMES | PRIVATE POSTS | PUBLIC POSTS |
| --------- | ------ | --------- | ----------- | ---------- | ------------- | ------------ |
| jon | jon@mydomain.com | 123 | Jon | Snow |  |  |
| tyrion | tyrion@mydomain.com | 123 | Tyrion | Lanister | This is Tyrion's private post. | This is Tyrion's public post. |
| sansa | sansa@mydomain.com | 123 | Sansa | Stark |  | Travel to the North. |
| thehound | thehound@mydomain.com | 123 | Sandor | Clegane | Eat more chicken. | Tyrion is short. |
| admin | admin@mydomain.com | 123 | Jrr | Martin |  |  |


### 2.4 Test Execution Report

This documents contains the test results and the summary of test execution activities;

#### **POSTMAN Execution Report**
**Name**: TravelPoint-Postman-Summary-Report.html

**URL**: [qa-social-network-app/reports/postman-newman-report](https://gitlab.com/stefanv877/qa-social-network-app/tree/master/reports/postman-newman-report)

#### Selenium Execution Report
**Name**: TravelPoint-Selenium-Summary-Report.html

**URL**: [qa-social-network-app/reports/selenium-runner-report](https://gitlab.com/stefanv877/qa-social-network-app/tree/master/reports/selenium-runner-report)


### 2.5 Test summary report

From defining various testing techniques and methodologies, to providing details
about the various defects, bugs, and discrepancies found in the software, this report offers a summary
of the entire testing process to the reader;

**Name**: TravelPoint_Test_Summary_Report.pdf

**URL**: [qa-social-network-app/reports/testrail-runs-report](https://gitlab.com/stefanv877/qa-social-network-app/blob/master/reports/TravelPoint_Test_Summary_Report.pdf)

### 2.6 Install/config guides

It defines system’s or program’s installation and configuration requirements, such
as the way it is set up, the components that make up the system, and its hardware & software
requirements;

Below you will find install/config guides for each of the automated tool:

- **POSTMAN**: [qa-social-network-app/postman](https://gitlab.com/stefanv877/qa-social-network-app/tree/master/postman)

- **SELENIUM**: [qa-social-network-app/social-network-test-framework](https://gitlab.com/stefanv877/qa-social-network-app/tree/master/social-network-test-framework)

